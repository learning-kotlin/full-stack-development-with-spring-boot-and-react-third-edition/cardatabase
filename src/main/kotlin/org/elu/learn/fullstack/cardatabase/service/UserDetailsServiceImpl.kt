package org.elu.learn.fullstack.cardatabase.service

import org.elu.learn.fullstack.cardatabase.domain.UserRepository
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl(private val userRepository: UserRepository): UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails {
        return userRepository.findByUsername(username)?.let {
            User.builder()
                .username(it.username)
                .password(it.password)
                .roles(it.role)
                .build()
        } ?: throw UsernameNotFoundException("User not found")
    }
}
