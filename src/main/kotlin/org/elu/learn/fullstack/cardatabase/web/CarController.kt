package org.elu.learn.fullstack.cardatabase.web

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/cars")
class CarController(private val carService: CarService) {
    @GetMapping
    fun getCars(): List<CarDto> = carService.getAll()

    @GetMapping("{id}")
    fun getCar(@PathVariable id: Long) = carService.getById(id)

    @PostMapping
    fun createCar(@RequestBody car: CarDto) = carService.create(car)

    @PutMapping("{id}")
    fun updateCar(@PathVariable id: Long, @RequestBody car: CarDto) = carService.update(id, car)

    @DeleteMapping("{id}")
    fun deleteCar(@PathVariable id: Long) = carService.delete(id)
}
