package org.elu.learn.fullstack.cardatabase.domain

import jakarta.persistence.CascadeType
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.OneToMany

@Entity
class Owner(
    @Id
    @GeneratedValue
    val id: Long? = null,
    var firstName: String,
    var lastName: String,
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "owner")
    val cars: List<Car>,
)
