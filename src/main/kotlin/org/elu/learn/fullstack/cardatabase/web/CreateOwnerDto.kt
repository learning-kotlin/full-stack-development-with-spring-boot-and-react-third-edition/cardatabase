package org.elu.learn.fullstack.cardatabase.web

data class CreateOwnerDto(
    val firstName: String,
    val lastName: String
)
