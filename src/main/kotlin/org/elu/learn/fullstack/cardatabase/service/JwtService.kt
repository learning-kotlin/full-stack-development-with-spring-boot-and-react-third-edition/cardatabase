package org.elu.learn.fullstack.cardatabase.service

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class JwtService {
    fun getToken(username: String,
                 additionalClaims: Map<String, Any> = emptyMap()): String =
        Jwts.builder()
            .claims()
            .subject(username)
            .expiration(Date.from(Instant.now().plusMillis(EXPIRATION_TIME)))
            .add(additionalClaims)
            .and()
            .signWith(KEY)
            .compact()

    fun getAuthUser(token: String): String? =
            Jwts.parser()
                .verifyWith(KEY)
                .build()
                .parseSignedClaims(token)
                .payload
                .subject

    fun buildAuthHeader(token: String) = "Bearer $token"

    companion object {
        const val EXPIRATION_TIME: Long = 86400000 // 1 day in ms
        private val KEY = Keys.hmacShaKeyFor("SignatureAlgorithm.HS256Abra4986720Cadabra586967896787".toByteArray())
    }
}
