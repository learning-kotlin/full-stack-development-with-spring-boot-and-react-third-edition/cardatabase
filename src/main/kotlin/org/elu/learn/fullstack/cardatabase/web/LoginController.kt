package org.elu.learn.fullstack.cardatabase.web

import org.elu.learn.fullstack.cardatabase.domain.AccountCredentials
import org.elu.learn.fullstack.cardatabase.service.JwtService
import org.elu.learn.fullstack.cardatabase.service.UserDetailsServiceImpl
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class LoginController(
    private val jwtService: JwtService,
    private val authenticationManager: AuthenticationManager,
    private val userDetailsService: UserDetailsServiceImpl
) {
    @PostMapping("/login")
    fun getToken(@RequestBody credentials: AccountCredentials): ResponseEntity<Unit> {
        authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(credentials.username, credentials.password)
        )

        val user = userDetailsService.loadUserByUsername(credentials.username)
        // generate token
        val jwt = jwtService.getToken(user.username)
        return ResponseEntity.ok()
            .header(HttpHeaders.AUTHORIZATION, jwtService.buildAuthHeader(jwt))
            .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "Authorization")
            .build()
    }
}
