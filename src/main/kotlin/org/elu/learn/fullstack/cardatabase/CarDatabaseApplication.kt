package org.elu.learn.fullstack.cardatabase

import org.elu.learn.fullstack.cardatabase.domain.Car
import org.elu.learn.fullstack.cardatabase.domain.CarRepository
import org.elu.learn.fullstack.cardatabase.domain.Owner
import org.elu.learn.fullstack.cardatabase.domain.OwnerRepository
import org.elu.learn.fullstack.cardatabase.domain.User
import org.elu.learn.fullstack.cardatabase.domain.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@SpringBootApplication
class CarDatabaseApplication {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(CarDatabaseApplication::class.java)
    }
}

@Configuration
class AppConfiguration {
    @Bean
    fun databaseInitializer(repository: CarRepository,
                            ownerRepository: OwnerRepository,
                            userRepository: UserRepository) = ApplicationRunner {
        val encoder = BCryptPasswordEncoder()
        val owner1 = Owner(firstName = "Ed", lastName = "Suisse", cars = listOf())
        val owner2 = Owner(firstName = "Janna", lastName = "Suisse", cars = listOf())
        ownerRepository.saveAll(listOf(owner1, owner2))

        repository.save(Car(
            brand = "Mercedes",
            model = "GLA 220",
            color = "Black",
            registerNumber = "ABC-1234",
            year = 2019,
            price = 32000,
            owner = owner1,
        ))
        repository.save(Car(
            brand = "Mercedes",
            model = "GLC 320",
            color = "Silver",
            registerNumber = "ABC-2345",
            year = 2020,
            price = 42000,
            owner = owner1,
        ))
        repository.save(Car(
            brand = "Volvo",
            model = "XC-90",
            color = "White",
            registerNumber = "ABC-3456",
            year = 2020,
            price = 52000,
            owner = owner2,
        ))

        repository.findAll().forEach { CarDatabaseApplication.logger.info("${it.brand} ${it.model}") }

        // Username: user, password: user
        userRepository.save(User(
            username = "user",
            password = encoder.encode("user"),
            role = "USER"))
        // Username: admin, password: admin
        userRepository.save(User(
            username = "admin",
            password = encoder.encode("admin"),
            role = "ADMIN"))
    }
}

fun main(args: Array<String>) {
    runApplication<CarDatabaseApplication>(*args)
    CarDatabaseApplication.logger.info("Application started")
}
