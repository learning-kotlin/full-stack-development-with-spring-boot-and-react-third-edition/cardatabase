package org.elu.learn.fullstack.cardatabase.web

data class CarDto(
    val id: Long? = null,
    val brand: String,
    val model: String,
    val color: String,
    val registerNumber: String,
    val year: Int,
    val price: Int,
    val owner: String
)
