package org.elu.learn.fullstack.cardatabase.web

data class OwnerDto(
    val id: Long? = null,
    val firstName: String,
    val lastName: String,
    val cars: List<CarDto>,
)
