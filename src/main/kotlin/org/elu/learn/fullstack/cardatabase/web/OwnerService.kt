package org.elu.learn.fullstack.cardatabase.web

import org.elu.learn.fullstack.cardatabase.domain.OwnerRepository
import org.springframework.stereotype.Service

@Service
class OwnerService(private val ownerRepository: OwnerRepository, private val mapper: AppMapper) {
    fun getAll() = ownerRepository.findAll().map { mapper.map(it) }

    fun getById(id: Long): OwnerDto = ownerRepository.findById(id)
        .map { mapper.map(it) }
        .orElseThrow { throw IllegalArgumentException("not found") }

    fun create(owner: CreateOwnerDto): OwnerDto {
        val created = ownerRepository.save(mapper.map(owner))
        return mapper.map(created)
    }

    fun update(id: Long, owner: CreateOwnerDto): OwnerDto {
        val found = ownerRepository.findById(id).orElseThrow { throw IllegalArgumentException("not found") }
        found.firstName = owner.firstName
        found.lastName = owner.lastName
        val saved = ownerRepository.save(found)
        return mapper.map(saved)
    }

    fun delete(id: Long) {
        ownerRepository.findById(id).orElseThrow { throw IllegalArgumentException("not found") }
        ownerRepository.deleteById(id)
    }
}
