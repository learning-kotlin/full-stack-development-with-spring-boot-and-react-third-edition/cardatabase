package org.elu.learn.fullstack.cardatabase.domain

data class AccountCredentials(
    val username: String,
    val password: String
)
